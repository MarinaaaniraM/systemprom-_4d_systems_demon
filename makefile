SRC = main.cpp

all: $(SRC:.cpp=.o)
		$(CC)  -o 4display main.o

%.o:% .cpp
		$(CC) -Os -c $<

.PHONY: clean
clean:
		$(RM) 4display *.o
