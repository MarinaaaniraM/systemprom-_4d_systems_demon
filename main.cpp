#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <unistd.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <ctype.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <time.h>
#include <syslog.h>

#define DEVICE_NAME         "/dev/ttyUSB0"

#define SOFT_VERSION_FILE   "/root/version"

#define Printf if(outputEnable)printf

// ============================================================================
// Типы команд (cmd)
#define REPORT_EVENT        0x7
#define ACK                 0x6
#define NAK                 0x15
#define WRITE_STR           0x2


// ============================================================================
// Типы графических эелементов
#define BUTTON              0x6
#define KEYBOARD            0xD
#define FORM                0xA
#define USER_BUTTON         0x21
#define FOUR_D_BUTTON       0x1E
#define GAUGE               0x0B
#define SCOPE               0x19
// Формы переключаются автоматически, ловим только сообщения о переключении.


// ============================================================================
// Номера кнопок на интерфейсе
// Кнопки для Form
#define SATELLITE_BUT       0x0
#define ETHERNET_0_BUT      0x1
#define ETHERNET_1_BUT      0x2
#define WIFI_BUT            0x3
#define LTE_BUT             0x4

#define CONTROL_BUT         0x5
#define STATISTICS_BUT      0x6
#define INFO_BUT            0x7
// Номера их форм отличаются на + 1 (в коде but + 1 используется)

// Кнопки UserButton
// Если у кнопок есть пара в виде стринга, то у полей такой же номер
#define IP_ETH_0_BUT        0x0
#define MASK_ETH_0_BUT      0x1
#define GATE_ETH_0_BUT      0x2

#define IP_ETH_1_BUT        0x3
#define MASK_ETH_1_BUT      0x4
#define GATE_ETH_1_BUT      0x5

#define IP_LOCAL_BUT        0x7
#define MASK_LOCAL_BUT      0x8
#define IP_DOC_SERVER_BUT   0xA
#define UPDATE_PERIOD_BUT   0x9


#define SAT_PRIORITY_BUT    0x0B
#define ETH_0_PRIORITY_BUT  0x0C
#define ETH_1_PRIORITY_BUT  0x0D
#define WIFI_PRIORITY_BUT   0x0E
#define LTE_PRIORITY_BUT    0x0F

// ============================================================================
// Номера форм
#define NUM_KEYS_FORM       9


// ============================================================================
// Toggle переключатели
#define ETH0_AUTO_TOGGLE    5
#define ETH1_AUTO_TOGGLE    6
#define ROUTING_TOGGLE      8
#define FIREWALL_TOGGLE     7

// ============================================================================
// Стринговые поля
#define SAT_STATISTICS      0x14
#define ETH_0_STATISTICS    0x13
#define ETH_1_STATISTICS    0x12
#define WIFI_STATISTICS     0x17
#define LTE_STATISTICS      0x1A

#define LOCAL_STATISTICS    0x15

#define SAT_SETTINGS        0x11
#define WIFI_SETTINGS       0x16
#define LTE_SETTINGS        0x19

#define SAT_IP_STR          0x6
#define WIFI_IP_STR         0x18
#define LTE_IP_STR          0x10

#define IP_LOCAL_STR        0x7
#define MASK_LOCAL_STR      0x8
#define IP_DOC_SERVER_STR   0xA
#define UPDATE_PERIOD_STR   0x9

#define NUMERIC_KEYS_LINE   29
//#define CHARS_KEYS_LINE     0xE

#define UP_TIME_STR         27
#define SOFT_VERSION_STR    28


// ============================================================================
// Длина reportEvent сообщения
#define REPORT_EVENT_SIZE   6


// ============================================================================
// Длина strings
#define STRINGS_LINE        16          // Узкие поля ввода
#define STRINGS_AREA        73          // Широкие поля для статистики


// ============================================================================
// Директории
#define STATISTICS_BUT_PATH        "/sys/class/net/"
#define WWW_PATH                   "../www/"
#define INTERFACE_PATH             "/sys/class/net/"
#define UPTIME_PATH                "/proc/uptime"
#define ROUTING_PATH               "/proc/sys/net/ipv4/ip_forward"
#define CPU_PATH                   "/proc/stat"
#define MEMINFO_PATH               "/proc/meminfo"
#define STATISTICS_FOLDER          "/statistics/"

#define INTERFACE_SPEED_FILE       "speed"

#define SATELLITE_BUT_CONFIG_FILE  "sat-op"
#define WIFI_BUT_CONFIG_FILE       "wlan0-op"
#define LTE_BUT_CONFIG_FILE        "3g-wan-op"

#define SAT_LEVEL_FILE             "sat"
#define WIFI_LEVEL_FILE            "wlan0"
#define LTE_LEVEL_FILE             "3g-wan"

#define CHECK_SATELLITE_SCRIPT     "checksattelite.php"
#define CONFIRM_SATELLITE_SCRIPT   "confirmsattelite.php"
#define CANCEL_SATELLITE_SCRIPT    "cancelsattelite.php"

#define FIREWALL_SCRIPT            "fw"

// Roman. Network reload (only needed operations)
#define NET_RELOAD_SCRIPT          "/etc/init.d/network reload"

#define NOIP_STRING				   "HE 3ADAHO"

char* statisticFiles[5]= {         "rx_bytes",   "tx_bytes", "rx_packets",
                                   "tx_packets", "rx_errors"};


// ============================================================================
#define CH_NUM 5
// Название интерфейсов
#define SAT                        "X1"
#define ETH_0                      "X2"
#define ETH_1                      "X3"
#define WIFI                       "wlan0"
#define LTE                        "3g-wan"
#define LTE_CONF                   "wan"

#define LOC                        "eth0"
#define DOC                        "eth0"

char* CH_NAMES[CH_NUM] = {SAT,ETH_0,ETH_1,WIFI,LTE};
char* CH_NAMES_CONF[CH_NUM] = {SAT,ETH_0,ETH_1,WIFI,LTE_CONF};
// ============================================================================
// Поля ввода
char currStr[STRINGS_AREA];
char strWithButtons[30][STRINGS_AREA];
char strWithoutButtons[30][STRINGS_AREA];

char strSettings[8][STRINGS_AREA];
char strStat[8][STRINGS_AREA];
char password[STRINGS_AREA];
char updatePeriod[2] = "3";
int currUserBut = 0;


// ============================================================================
static int currForm = 0;
int isAuto[CH_NUM] = {1,1,1,1,1};
int isFormChanging = 0;
unsigned int workingTime = 0;

static int prevUserCPU = 0;
static int prevNiceCPU = 0;
static int prevSystemCPU = 0;
static int prevIdleCPU = 0;
static int prevIowaitCPU = 0;
static int prevIrqCPU = 0;
static int prevSoftirqCPU = 0;

int outputEnable = 0;

char deviceName[80];
char SOFT_VERSION[80];

struct ifaddrs *ifaddr=NULL;      // global list of all net interfaces

volatile int STOP = false;
volatile int wait_flag = false;

long BAUD = B115200;              // derived baud rate from command line
long DATABITS = CS8;
long STOPBITS = 0;
long PARITYON = 0;
long PARITY = 0;

int getdefaultgateway(const char *ifn, int* metric );

// ============================================================================
//definition of I/O signal handler
void signal_handler_IO (int status)
{
    // Printf("received SIGIO signal.\n");
    // wait_flag = false;
}

//definition of FINAL handler
void signal_handler_TERM (int status)
{
    // Printf("received SIGTERM or SIGINT signal.\n");
   STOP = true;
}

//definition of CHILD signal handler
void signal_handler_CHLD (int status)
{
    // Printf("received SIGCHLD signal.\n");
   wait_flag = true;
}

// ============================================================================
// Контрольная сумма
int checksum(char* data, int len)
{
    int chksum = 0;
    for (int i = 0; i < len - 1; ++i)
    {
        chksum = chksum ^ data[i];
    }
    return chksum;
}


// ============================================================================
void strings(int fd, char* str, char strNum)
{
    int stringSize = strlen(str);
    int msgSize = 5 + stringSize;
    char msg[80];

    msg[0] = WRITE_STR;
    msg[1] = strNum;
    msg[2] = stringSize+1;
    for (int i = 0; i < stringSize; ++i)
    {
        msg[i + 3] = str[i];
    }
    msg[msgSize - 2] = '\0';
    msg[msgSize - 1] = checksum(msg, msgSize);

    write(fd, msg, msgSize);

    // Печатаем само сообщение
    Printf("[ ");
    for (unsigned int i = 0; i < msgSize; ++i)
    {
        Printf("%02X ", msg[i]);
    }
    Printf("]\n");
}


// ============================================================================
void setToggle(int fd, char num, char state)
{
    int written = 0;
    char msg[REPORT_EVENT_SIZE];
    msg[0] = 1;
    msg[1] = FOUR_D_BUTTON;
    msg[2] = num;
    msg[3] = 0;
    msg[4] = state;
    msg[5] = msg[0] ^ msg[1] ^ msg[2] ^ msg[3] ^ msg[4];
    written = write(fd, msg, sizeof(msg));

    if (written < REPORT_EVENT_SIZE)
        Printf(">> Error opepation 'setToggle'.\n");
    else
        Printf(">> Toggle set.\n");
}


// ============================================================================
void setGauge(int fd,char numGauge, char level)
{
    int written = 0;
    char msg[REPORT_EVENT_SIZE];
    msg[0] = 1;
    msg[1] = GAUGE;
    msg[2] = numGauge;
    msg[3] = 0;
    msg[4] = level;
    msg[5] = msg[0] ^ msg[1] ^ msg[2] ^ msg[3] ^ msg[4];
    written = write(fd, msg, sizeof(msg));

    if (written < REPORT_EVENT_SIZE)
        Printf(">> Error opepation.\n");
    else
        Printf(">> Gauge set.\n");
}


// ============================================================================
void setScope(int fd, char numScope, char value)
{
    int written = 0;
    char msg[REPORT_EVENT_SIZE];
    msg[0] = 1;
    msg[1] = SCOPE;
    msg[2] = numScope;
    msg[3] = 0;
    msg[4] = value;
    msg[5] = msg[0] ^ msg[1] ^ msg[2] ^ msg[3] ^ msg[4];
    written = write(fd, msg, sizeof(msg));


    if (written < REPORT_EVENT_SIZE)
        Printf(">> Error scope set.\n");
    else
        Printf(">> Scope set.\n");
}


// ============================================================================
void readSettings(int fd, char* fileName, char numForm)
{
    FILE* file;
    char path[30];
    sprintf(path, "%s%s", WWW_PATH, fileName);

    file = fopen(path, "r");
    if (!file)
    {
        Printf(">> Error reading settings: ");
        Printf("%s", path);
        Printf("\n\n_______________________________________\n\n");
    }
    else
    {
        //fscanf(file, "%s", strSettings[numForm - 1]);
        // Roman Чтобы читать всю строку, а не до первого пробела
        char currSet [STRINGS_AREA];
        fgets(currSet, STRINGS_AREA, file);
        fclose(file);
        if (strcmp(strSettings[numForm - 1], currSet) != 0 || isFormChanging)
        {
            strncpy(strSettings[numForm - 1], currSet, STRINGS_LINE);
            switch(numForm)
            {
            case SATELLITE_BUT + 1:
                strings(fd, strSettings[numForm - 1], SAT_SETTINGS); break;
            case WIFI_BUT + 1:
                strings(fd, strSettings[numForm - 1], WIFI_SETTINGS); break;
            case LTE_BUT + 1:
                strings(fd, strSettings[numForm - 1], LTE_SETTINGS); break;
            }
        }
    }
}


// ============================================================================
void readStatistics(int fd, char numForm)
{
    char path[60];
    memset(path, 0, sizeof(path));
    int stringsNum = 0;
    switch (numForm)
    {
    case SATELLITE_BUT + 1:
        sprintf(path, "%s%s%s", STATISTICS_BUT_PATH, SAT, STATISTICS_FOLDER);
        stringsNum = SAT_STATISTICS;
        break;
    case ETHERNET_0_BUT + 1:
        sprintf(path, "%s%s%s", STATISTICS_BUT_PATH, ETH_0, STATISTICS_FOLDER);
        stringsNum = ETH_0_STATISTICS;
        break;
    case ETHERNET_1_BUT + 1:
        sprintf(path, "%s%s%s", STATISTICS_BUT_PATH, ETH_1, STATISTICS_FOLDER);
        stringsNum = ETH_1_STATISTICS;
        break;
    case WIFI_BUT + 1:
        sprintf(path, "%s%s%s", STATISTICS_BUT_PATH, WIFI, STATISTICS_FOLDER);
        stringsNum = WIFI_STATISTICS;
        break;
    case LTE_BUT + 1:
        sprintf(path, "%s%s%s", STATISTICS_BUT_PATH, LTE, STATISTICS_FOLDER);
        stringsNum = LTE_STATISTICS;
        break;
    case STATISTICS_BUT + 1:
        sprintf(path, "%s%s%s", STATISTICS_BUT_PATH, LOC, STATISTICS_FOLDER);
        stringsNum = LOCAL_STATISTICS;
        break;
    }
    memset(strStat[numForm - 1], 0, STRINGS_AREA);
    for (int i = 0; i < 5; ++i)
    {
        FILE* file;
        char currPath[60];
        memset(currPath, 0, sizeof(currPath));
        sprintf(currPath, "%s%s", path, statisticFiles[i]);

        file = fopen(currPath, "r");
        if (!file)
        {
            Printf(">> Error reading file: ");
            Printf("%s", path);
            Printf("\n\n_______________________________________\n\n");
        }
        else
        {
            char tempBuf[STRINGS_AREA];
            fscanf(file, "%s", tempBuf);
            fclose(file);
            sprintf(strStat[numForm - 1], "%s\n%s\n",
                    strStat[numForm - 1], tempBuf);
        }
    }
    strings(fd, strStat[numForm - 1], stringsNum);
}

// ============================================================================
char* getVersion()
{
    FILE* file;

    file = fopen(SOFT_VERSION_FILE, "r");
    if (!file)
    {
        strcpy(SOFT_VERSION,"KSPPI, 0.1A");
    }
    else
    {
        fgets(SOFT_VERSION, sizeof(SOFT_VERSION),file);
        fclose(file);
    }

    return SOFT_VERSION;
}

// ============================================================================
int getCPU()
{
    FILE* file;
    char cpu[4];
    int user = 0;
    int nice = 0;
    int system = 0;
    int idle = 0;
    int iowait = 0;
    int irq = 0;
    int softirq = 0;
    int totalCPU = 0;

    file = fopen(CPU_PATH, "r");
    if (!file)
    {
        Printf(">> Error reading CPU: ");
        Printf("%s", CPU_PATH);
        Printf("\n\n_______________________________________\n\n");
    }
    else
    {
        fscanf(file, "%s %d %d %d %d %d %d %d",
               cpu, &user, &nice, &system, &idle, &iowait, &irq, &softirq);
        fclose(file);
    }

    totalCPU = (user - prevUserCPU + nice - prevNiceCPU + system - prevSystemCPU
                + iowait - prevIowaitCPU + irq - prevIrqCPU +
                softirq - prevSoftirqCPU) * 100  /
               (user - prevUserCPU + nice - prevNiceCPU + system - prevSystemCPU
                + idle - prevIdleCPU + iowait - prevIowaitCPU + irq - prevIrqCPU
                + softirq - prevSoftirqCPU);

    prevUserCPU = user;
    prevNiceCPU = nice;
    prevSystemCPU = system;
    prevIdleCPU = idle;
    prevIowaitCPU = iowait;
    prevIrqCPU = irq;
    prevSoftirqCPU = softirq;

    return totalCPU;
}


// ============================================================================
int getMemory()
{
    FILE* file;

    char memTotal[10];
    char memFree[10];
    char kB[3];
    int total = 0;
    int free = 0;
    int memoryInfo = 0;

    file = fopen(MEMINFO_PATH, "r");
    if (!file)
    {
        Printf(">> Error reading memory info: ");
        Printf("%s", MEMINFO_PATH);
        Printf("\n\n_______________________________________\n\n");
    }
    else
    {
        fscanf(file, "%s %d %s \n %s %d %s",
               memTotal, &total, kB, memFree, &free, kB);
        fclose(file);
    }
    memoryInfo = (total - free) * 100 / total;

    return memoryInfo;
}


// ============================================================================
time_t getUpTime()
{
    FILE* file;
    char buffer[80];
    time_t upTime = 0;

    file = fopen(UPTIME_PATH, "r");
    if (!file)
    {
        Printf(">> Error reading upTime: ");
        Printf("%s", UPTIME_PATH);
        Printf("\n\n_______________________________________\n\n");
    }
    else
    {
        fgets(buffer, 80, file);
        sscanf(buffer, "%d", &upTime);
        fclose(file);
    }
    return upTime;
}


// ============================================================================
bool getEthRun(char* interface)
{
    struct ifreq ifr;
    int sd = -1;
    memset( &ifr, 0, sizeof(ifreq));
    sd = socket( AF_INET, SOCK_DGRAM, 0);
    strncpy( ifr.ifr_name, interface, IFNAMSIZ);
    if( ioctl( sd, SIOCGIFFLAGS, &ifr) == -1)
    {
        Printf(">> Error to check %s run.", interface);
        close( sd);
    }
    close(sd);
    return ((ifr.ifr_flags & IFF_RUNNING) != 0) ? 1 : 0;
}

// ============================================================================
int getEthSpeed(char* interface)
{
    FILE* file;
    char path[30];
    int speed = 0;
    // Roman Если кабель не подключен, скорость проверять бессмысленно
    if( !getEthRun( interface))
      return 0;

    sprintf(path, "%s%s/%s", INTERFACE_PATH, interface, INTERFACE_SPEED_FILE);

    file = fopen(path, "r");
    if (!file)
    {
        Printf(">> Error reading interface speed: ");
        Printf("%s", path);
        Printf("\n\n_______________________________________\n\n");
    }
    else
    {
        fscanf(file, "%d", &speed);
        fclose(file);
    }
    switch(speed)
    {
    case 10:
        return 1;
        break;
    case 100:
        return 2;
        break;
    case 1000:
        return 3;
        break;
    default: // Кабель подключен, но данных о скорости нет. Считаем что максимум.
        return 3;
    }
}


// ============================================================================
bool getIntState(char* interface)
{
    struct ifreq ifr;
    int sd = -1;
    memset( &ifr, 0, sizeof(ifreq));
    sd = socket( AF_INET, SOCK_DGRAM, 0);
    strncpy( ifr.ifr_name, interface, IFNAMSIZ);
    if( ioctl( sd, SIOCGIFFLAGS, &ifr) == -1)
    {
        Printf(">> Error to check %s state.", interface);
        close( sd);
    }
    close(sd);
    return ((ifr.ifr_flags & IFF_UP) != 0) ? 1 : 0;
}


// ============================================================================
// Roman Как правило нужно знать и признак автонастройки
bool getEthAuto(char* interface)
{
  FILE* pexe = NULL;
  char exe[64];
  sprintf(exe, "uci get network.%s.proto", interface);
  pexe = popen( exe, "r");
  fgets(exe,sizeof(exe),pexe);
  pclose(pexe);
  if(!strncmp( exe,"dhcp",4))
      return( 1);
  else
      return( 0);
}


// ============================================================================
int getLevel(char* levelFile)
{
    FILE* file;
    char path[30];
    int level = 0;
    sprintf(path, "%s%s", WWW_PATH, levelFile);

    file = fopen(path, "r");
    if (!file)
    {
        Printf(">> Error reading settings: ");
        Printf("%s", path);
        Printf("\n\n_______________________________________\n\n");
    }
    else
    {
        fscanf(file, "%d", &level);
        fclose(file);
        if (level > 5)
            level = 5;
    }
    return level;
}


// ============================================================================
bool setRouting()
{
    FILE* file;
    char path[30];
    int isRouting = 0;

    file = fopen(ROUTING_PATH, "rw");
    if (!file)
    {
        Printf(">> Error reading routing: ");
        Printf("%s", path);
        Printf("\n\n_______________________________________\n\n");
    }
    else
    {
        isRouting=fgetc(file) - 0x30;
        fclose(file);
    }
    return isRouting;
}


// ============================================================================
void setPriority(int num, int priority)
{
    char exe[80];
    // ищем другой канал с таким же приоритетом
    for (int i = SAT_PRIORITY_BUT; i < SAT_PRIORITY_BUT + 5; ++i)
    {
        if (atoi(strWithButtons[i]) == priority)
        {
            if (i == num)
                continue; // еще не нашли совпадения с другим каналом
            else
            {
                sprintf(exe, "uci set network.%s.metric='%s'",
                        CH_NAMES_CONF[i-SAT_PRIORITY_BUT], strWithButtons[num]);
                system(exe);
                memset(strWithButtons[i], 0, sizeof(strWithButtons[i]));
                strcpy(strWithButtons[i], strWithButtons[num]);
                break; // совпадение найдено и исправлено
            }
        }
    }
    // меняем приоритет нужного канала
    sprintf(exe, "uci set network.%s.metric='%d'",
            CH_NAMES_CONF[num-SAT_PRIORITY_BUT], priority);
    system(exe);
    memset(strWithButtons[num], 0, sizeof(strWithButtons[num]));
    sprintf(strWithButtons[num], "%d", priority);

    system(NET_RELOAD_SCRIPT);
    system("uci commit network");
}


// ============================================================================
bool setFirewall()
{
    FILE* file;
    char path[30];
    char answer[10];
    sprintf(path, "%s%s", WWW_PATH, FIREWALL_SCRIPT);
    file = popen("uci get firewall.@defaults[0].forward", "r");
    if (!file)
    {
        Printf(">> Error reading fw script: ");
        Printf("%s", path);
        Printf("\n\n_______________________________________\n\n");
    }
    else
    {
        fgets(answer, sizeof(answer), file);
        if (strcmp(answer, "ACCEPT") != 0)
            return false;
        fclose(file);
    }
    return true;
}


// ============================================================================
void formReloading(int fd, char numForm)
{
    switch(numForm)
    {
    // Номера форм сдвинуты на 1 из-за заставки в форме 0
    case SATELLITE_BUT + 1:
        readStatistics(fd, numForm);
        readSettings(fd, SATELLITE_BUT_CONFIG_FILE, numForm);
        setGauge(fd, SATELLITE_BUT, getLevel(SAT_LEVEL_FILE));
        break;
    case ETHERNET_0_BUT + 1:{
		int newAuto = getEthAuto(ETH_0);
        readStatistics(fd, numForm);
        setToggle(fd, ETHERNET_0_BUT, getIntState(ETH_0));
        if(newAuto != isAuto[ETHERNET_0_BUT]) {
			isAuto[ETHERNET_0_BUT] = newAuto;
			setToggle(fd, ETH0_AUTO_TOGGLE, isAuto[ETHERNET_0_BUT]);
		}
        setGauge(fd, ETHERNET_0_BUT, getEthSpeed(ETH_0));
        break;
	}
    case ETHERNET_1_BUT + 1: {
        int newAuto = getEthAuto(ETH_1);
        readStatistics(fd, numForm);
        setToggle(fd, ETHERNET_1_BUT, getIntState(ETH_1));
        isAuto[ETHERNET_1_BUT] = getEthAuto(ETH_1);
        if(newAuto != isAuto[ETHERNET_1_BUT]) {
            isAuto[ETHERNET_1_BUT] = newAuto;
            setToggle(fd, ETH1_AUTO_TOGGLE, isAuto[ETHERNET_1_BUT]);
        }
        setGauge(fd, ETHERNET_1_BUT, getEthSpeed(ETH_1));
        break;
	} 
    case WIFI_BUT + 1:
        readStatistics(fd, numForm);
        readSettings(fd, WIFI_BUT_CONFIG_FILE, numForm);
        setToggle(fd, WIFI_BUT, getIntState(WIFI));
        setGauge(fd, WIFI_BUT, getLevel(WIFI_LEVEL_FILE));
        break;

    case LTE_BUT + 1:
        readStatistics(fd, numForm);
        readSettings(fd, LTE_BUT_CONFIG_FILE, numForm);
        setToggle(fd, LTE_BUT, getIntState(LTE));
        setGauge(fd, LTE_BUT, getLevel(LTE_LEVEL_FILE));
        break;
    case STATISTICS_BUT + 1:
    {
        readStatistics(fd, numForm);

        time_t timetime = getUpTime();
        struct tm* timeinfo;
        char buffer [80];
        timeinfo = gmtime(&timetime);
        strftime (buffer,80,"%T",timeinfo);

        strings(fd, buffer, UP_TIME_STR);
        break;
    }
    case CONTROL_BUT + 1:
        setToggle(fd, ROUTING_TOGGLE, setRouting());
        setToggle(fd, FIREWALL_TOGGLE, setFirewall());
        break;
    case INFO_BUT + 1:
        setGauge(fd, 5, getLevel(SAT_LEVEL_FILE));
        setGauge(fd, 6, getEthSpeed(ETH_0));
        setGauge(fd, 7, getEthSpeed(ETH_1));
        setGauge(fd, 8, getLevel(WIFI_LEVEL_FILE));
        setGauge(fd, 9, getLevel(LTE_LEVEL_FILE));
        break;
    }
}


// ============================================================================
void formChanging(int fd, char numForm)
{
    switch(numForm)
    {
    // Номера форм сдвинуты на 1 из-за заставки в форме 0
    case SATELLITE_BUT + 1:
        readSettings(fd, SATELLITE_BUT_CONFIG_FILE, numForm);
        strings(fd, strWithoutButtons[SAT_IP_STR], SAT_IP_STR);
        break;
    case ETHERNET_0_BUT + 1:
        strings(fd, strWithButtons[IP_ETH_0_BUT], IP_ETH_0_BUT);
        strings(fd, strWithButtons[MASK_ETH_0_BUT], MASK_ETH_0_BUT);
        strings(fd, strWithButtons[GATE_ETH_0_BUT], GATE_ETH_0_BUT);
        isAuto[ETHERNET_0_BUT] = getEthAuto(ETH_0);
        setToggle(fd, ETH0_AUTO_TOGGLE, isAuto[ETHERNET_0_BUT]);
        break;
    case ETHERNET_1_BUT + 1:
        strings(fd, strWithButtons[IP_ETH_1_BUT], IP_ETH_1_BUT);
        strings(fd, strWithButtons[MASK_ETH_1_BUT], MASK_ETH_1_BUT);
        strings(fd, strWithButtons[GATE_ETH_1_BUT], GATE_ETH_1_BUT);
        isAuto[ETHERNET_1_BUT] = getEthAuto(ETH_1);
        setToggle(fd, ETH1_AUTO_TOGGLE, isAuto[ETHERNET_1_BUT]);
        break;
    case WIFI_BUT + 1:
        readSettings(fd, WIFI_BUT_CONFIG_FILE, numForm);
        strings(fd, strWithoutButtons[WIFI_IP_STR], WIFI_IP_STR);
        break;

    case LTE_BUT + 1:
        readSettings(fd, LTE_BUT_CONFIG_FILE, numForm);
        strings(fd, strWithoutButtons[LTE_IP_STR], LTE_IP_STR);
        break;
    case STATISTICS_BUT + 1:
        break;
    case CONTROL_BUT + 1:
        int priority[CH_NUM];
        memset( priority, 0 ,sizeof(priority));

        strings(fd, strWithButtons[IP_LOCAL_BUT], IP_LOCAL_BUT);
        strings(fd, strWithButtons[MASK_LOCAL_BUT], MASK_LOCAL_BUT);
        strings(fd, strWithButtons[IP_DOC_SERVER_BUT], IP_DOC_SERVER_BUT);

        for(int i = 0; i < CH_NUM; ++i)
        {
            getdefaultgateway(CH_NAMES[i], &priority[i]);
            sprintf(strWithButtons[SAT_PRIORITY_BUT + i], "%d", priority[i]);
            strings(fd, strWithButtons[SAT_PRIORITY_BUT + i],
                    SAT_PRIORITY_BUT + i);
        }
        strings(fd, updatePeriod, UPDATE_PERIOD_STR);

        break;
    case INFO_BUT + 1:
        strings(fd, SOFT_VERSION, SOFT_VERSION_STR);
        break;
    }
    isFormChanging = 0;

    int sel = 0;
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 100000;
    sel = select(NULL, NULL, NULL, NULL, &tv);
    if (sel == 0)
    {
        formReloading(fd, numForm);
    }
}


// ============================================================================
// Переключение формы
void changedForm(int fd, char num)
{
    int written = 0;
    char msg[REPORT_EVENT_SIZE];
    msg[0] = 1;
    msg[1] = FORM;
    msg[2] = num;
    msg[3] = 0;
    msg[4] = 0;
    msg[5] = msg[0] ^ msg[1] ^ msg[2] ^ msg[3] ^ msg[4];
    written = write(fd, msg, sizeof(msg));

    if (written < REPORT_EVENT_SIZE)
        Printf(">> Error opepation.\n");
    else
        Printf(">> Form changed.\n");

    currForm = num;

    if (num <= 8)             // Основные формы, для выведения информации,
        isFormChanging = 1;   // ждем успешного завершение смены формы
}


// ============================================================================
void setConfiguration(int line)
{
    char exe[80];
    switch(currUserBut)
    {
    case IP_ETH_0_BUT:
        sprintf(exe, "uci set network.%s.ipaddr='%s'", ETH_0,
                strWithButtons[line]);
        system(exe);
        break;
    case MASK_ETH_0_BUT:
        sprintf(exe, "uci set network.%s.netmask='%s'", ETH_0,
                strWithButtons[line]);
        system(exe);
        break;
    case GATE_ETH_0_BUT:
		sprintf(exe, "uci set network.%s.gateway='%s'", ETH_0,
                strWithButtons[line]);
        Printf("%s\n",exe);
        system(exe);
        break;
    case GATE_ETH_1_BUT:
        sprintf(exe, "uci set network.%s.gateway='%s'", ETH_1,
                strWithButtons[line]);
        Printf("%s\n",exe);
        system(exe);
        break;
    case IP_ETH_1_BUT:
        sprintf(exe, "uci set network.%s.ipaddr='%s'", ETH_1,
                strWithButtons[line]);
        system(exe);
        break;
    case MASK_ETH_1_BUT:
        sprintf(exe, "uci set network.%s.netmask='%s'", ETH_0,
                strWithButtons[line]);
        system(exe);
        break;
    case IP_LOCAL_BUT:
        sprintf(exe, "uci set network.%s.ipaddr='%s'", LOC,
                strWithButtons[line]);
        system(exe);
        break;
    case MASK_LOCAL_BUT:
        sprintf(exe, "uci set network.%s.netmask='%s'", LOC,
                strWithButtons[line]);
        system(exe);
        break;
    }
    system(NET_RELOAD_SCRIPT);
    system("uci commit network");
}


// ============================================================================
void numKeyboardProcessing(int fd, char* buf)
{
    switch (buf[4])
    {
    case 0x0A:          // Кнопка ОК
    {
        // Roman Флаг необходимости реконфигурации (сравнение строк)
        int reconf = strcmp(strWithButtons[currUserBut], currStr);
        if(reconf != 0)
        {
            if (currUserBut == UPDATE_PERIOD_BUT)
                strncpy(updatePeriod, currStr, sizeof(currStr));

            else if (currUserBut >= SAT_PRIORITY_BUT &&
                     currUserBut <= LTE_PRIORITY_BUT)
                setPriority(currUserBut, atoi(currStr));
            else
            {
                strncpy(strWithButtons[currUserBut], currStr,
                        sizeof(strWithButtons[currUserBut]));
                setConfiguration(currUserBut);
            }
        }
        changedForm(fd, currForm);
        break;
    }
    case 0x1B:          // Кнопка Отмена
        memset(currStr, 0, strlen(currStr));
        changedForm(fd, currForm);
        break;
    case 0x08:          // Кнопка Стереть
        if (strlen(currStr) > 0)
        {
            currStr[strlen(currStr) - 1] = '\0';
            strings(fd, currStr, NUMERIC_KEYS_LINE);
        }
        break;
    default:            // Остальный кнопки
        if (currUserBut == UPDATE_PERIOD_BUT)
        {
            if (strlen(currStr) < 2)
                currStr[strlen(currStr)] = buf[4];
        }
        else if (currUserBut >= SAT_PRIORITY_BUT &&
                 currUserBut <= LTE_PRIORITY_BUT)
        {
            if (strlen(currStr) < 1)
                currStr[strlen(currStr)] = buf[4];
        }
        else
        {
            if (strlen(currStr) < STRINGS_LINE)
                currStr[strlen(currStr)] = buf[4];
        }
        strings(fd, currStr, NUMERIC_KEYS_LINE);
    }
}


// ============================================================================
void formProcessing(int fd, char numButton)
{
    // isAutoEth действует только в ETH
    if ( !((currForm==2 || currForm==3) && isAuto[currForm-1]) )
        {
            int saveForm = currForm;
            currUserBut = numButton;
            for (unsigned int i = 0;
                 i < sizeof(strWithButtons[currUserBut]); ++i)
            {
                if (numButton == UPDATE_PERIOD_BUT)
                    currStr[i] = updatePeriod[i];
                else
                    currStr[i] = strWithButtons[currUserBut][i];
            }
            changedForm(fd, NUM_KEYS_FORM);
            currForm = saveForm; // Roman restoring value of currForm
            strings(fd, currStr, NUMERIC_KEYS_LINE);
        }
}


// ============================================================================
void toggleProcessing(int fd, char* buf)
{
    char exe[80];

    memset(exe, 0, sizeof(exe));
    switch(buf[2])
    {
    case ETHERNET_0_BUT:
         if (buf[4]) { // включение интерфеса
           sprintf(exe, "ifconfig %s up", ETH_0);
           system(exe);
		   if( !isAuto[ETHERNET_0_BUT] ) // не DHCP
           {
             // Нужно поднять маршрут по умолчанию,
             // который задан в конфиге (сам не поднимется)
			 FILE* f = NULL;
			 char gwstring[32];
			 int metric=0;
			 gwstring[0] = '\0';
			 // получаем маршрут
			 sprintf(exe, "uci get network.%s.gateway", ETH_0);
			 //Printf( exe);
			 f = popen( exe, "r");
             if( f) {
               fgets(gwstring, sizeof(gwstring), f); // getting gw from config
               pclose(f);
               if( strlen(gwstring) > 0) { // маршрут задан
                // Printf( gwstring);
                 gwstring[strlen(gwstring)-1] = 0;
                 // уточним метрику интерфейса (сама не привязывается к маршруту)
                 getdefaultgateway( ETH_0, &metric);
				 // поднимаем маршрут
				 sprintf(exe, "route add default gw %s metric %d %s",
						 gwstring, metric, ETH_0);
                 //Printf( exe);
				 system( exe);
			   }
             }
		   }
         } else {
           sprintf(exe, "ifconfig %s down", ETH_0);
           system(exe);
	     }
         break;
    case ETHERNET_1_BUT:
         if (buf[4]){ // включение интерфеса
           sprintf(exe, "ifconfig %s up", ETH_1);
           system(exe);
		   if( !isAuto[ETHERNET_1_BUT] ) // не DHCP
           {
             // Нужно поднять маршрут по умолчанию,
             // который задан в конфиге (сам не поднимется)
			 FILE* f = NULL;
			 char gwstring[32];
			 int metric=0;
			 gwstring[0] = '\0';
			 // получаем маршрут
			 sprintf(exe, "uci get network.%s.gateway", ETH_1);
			 f = popen( exe, "r");
             if( f) {
               fgets(gwstring, sizeof(gwstring), f); // getting gw from config
               pclose(f);
               if( strlen(gwstring) > 0) { // маршрут задан
                 gwstring[strlen(gwstring)-1] = 0;
                 // уточним метрику интерфейса (сама не привязывается к маршруту)
                 getdefaultgateway( ETH_1, &metric);
				 // поднимаем маршрут
				 sprintf(exe, "route add default gw %s metric %d %s",
						 gwstring, metric, ETH_1);
				 system( exe);
			   }
             }
		   }
         } else {
            sprintf(exe, "ifconfig %s down", ETH_1);
			system(exe);
		 }
         break;
    case ETH0_AUTO_TOGGLE:
        isAuto[ETHERNET_0_BUT] = buf[4] ? 1 : 0;
        if(isAuto[ETHERNET_0_BUT])
            sprintf(exe, "uci set network.%s.proto='dhcp'", ETH_0);
        else
            sprintf(exe, "uci set network.%s.proto='static'", ETH_0);
        system(exe);
        system(NET_RELOAD_SCRIPT);
        break;
    case ETH1_AUTO_TOGGLE:
        isAuto[ETHERNET_1_BUT] = buf[4] ? 1 : 0;
        if(isAuto[ETHERNET_1_BUT])
            sprintf(exe, "uci set network.%s.proto='dhcp'", ETH_1);
        else
            sprintf(exe, "uci set network.%s.proto='static'", ETH_1);
        system(exe);
        system(NET_RELOAD_SCRIPT);
        break;
    case SATELLITE_BUT:
        if (buf[4])
            sprintf(exe, "php -n -f %s%s", WWW_PATH, CONFIRM_SATELLITE_SCRIPT);
        else
            sprintf(exe, "php -n -f %s%s", WWW_PATH, CANCEL_SATELLITE_SCRIPT);
        sprintf(exe, "php -n -f %s%s", WWW_PATH, CHECK_SATELLITE_SCRIPT);
        if(!fork()) {
          system(exe);
          exit(0);
	    }
        break;
    case ROUTING_TOGGLE:
        FILE* file;
        int isRouting = 0;
        file = fopen(ROUTING_PATH, "r+");
        if (!file)
        {
            Printf(">> Error reading routing: ");
            Printf("%s", ROUTING_PATH);
            Printf("\n\n_______________________________________\n\n");
        }
        else
        {
            fscanf(file, "%d", &isRouting);

            fseek(file, 0, SEEK_SET);
            fprintf(file, "%d", !isRouting);
            fclose(file);
        }
    }
    system("uci commit network");
}

// ============================================================================
void reportEvent(int fd)
{
    char buf[REPORT_EVENT_SIZE];
    memset(buf, 0, sizeof(buf));
    buf[0] = REPORT_EVENT;
    int readed = 0;
    while(readed < 5)           // Message
    {
        int rd = 0;
        rd = read(fd, buf + 1 + readed, 5 - readed);
        if (rd > 0)
            readed += rd;
    }

    // ------------------------------------------------------------------------
    // Печатаем само сообщение
    Printf("[ ");
    for (unsigned int i = 0; i < sizeof(buf); ++i)
    {
        Printf("%02X ", buf[i]);
    }
    Printf("]\n");

    // ------------------------------------------------------------------------
    // Если контрольна сумма правильная, то работаем дальше
    if (checksum( buf, sizeof(buf) ) == buf[sizeof(buf) - 1] )
    {
        switch(buf[1])
        {
        case BUTTON:
            changedForm(fd, (buf[2] & 7) + 1);
            break;
        case KEYBOARD:
                numKeyboardProcessing(fd, buf);
            break;
        case USER_BUTTON:
            formProcessing(fd, buf[2]);  break;
        case FOUR_D_BUTTON:
            toggleProcessing(fd, buf);
        }
    }
    else
        Printf(">> Checksum error.\n");
}


// ============================================================================
// Читаем команду от экрана
void readFromDevice(int fd)
{
    char cmd = 0;
    int readed = 0;
    readed = read(fd, &cmd, 1);
    if (readed == 1)                // Если прочитали cmd
    {
        switch (cmd)
        {
        case ACK:
            Printf(">> Reciever has correctly recieved.\n");
            if (isFormChanging)     // Только после этого заполняем поля данными
            {
                formChanging(fd, currForm);
            }
            break;
        case NAK:
            Printf(">> Reciever has NOT correctly received.\n");
            cmd=0;
            // защита от ошибок - заполняем порт байтами пока устройство не придет в норму
            while( read(fd, &cmd, 1)<=0)
             write( fd, &cmd, 1);
            break;
        case REPORT_EVENT:
            reportEvent(fd);
            break;
        default:
            Printf(">> Unknown command: %d!\n", cmd);
            //Printf("\n_______________________________________\n\n");
        }
    }
}


// ============================================================================
// Returning IPv4 address by iface name
int getIfaceAddr(const char *ifn)
{
   struct ifaddrs *ifa=NULL;
   int family=0, s=0;
   char host[NI_MAXHOST];
// Now free ifadr each time calling. In future - check the time
   if( ifaddr)
    freeifaddrs(ifaddr);

   if ( !ifn || getifaddrs(&ifaddr) == -1) {
               perror("getifaddrs");
               return 0;
   }

           /* Walk through linked list, maintaining head pointer so we
              can free list later */

   for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
               if (ifa->ifa_addr == NULL)
                   continue;

               family = ifa->ifa_addr->sa_family;

               /* Display interface name and family (including symbolic
                  form of the latter for the common families) */

               /* For an AF_INET* interface address, display the address */

         if ((family == AF_INET || family == AF_INET6) &&
              !strcmp( ifa->ifa_name, ifn))
         {
          s =  ((struct sockaddr_in *)ifa->ifa_addr)->sin_addr.s_addr;
          break;
         }
    }
    return(s);
}


// ============================================================================
// Returning IPv4 mask by iface name
int getIfaceMask(const char *ifn)
{
   struct ifaddrs *ifa=NULL;
   int family=0, s=0;
   char host[NI_MAXHOST];
// Now free ifadr each time calling. In future - check the time
   if( ifaddr)
    freeifaddrs(ifaddr);

   if ( !ifn || getifaddrs(&ifaddr) == -1) {
               perror("getifaddrs");
               return 0;
   }

           /* Walk through linked list, maintaining head pointer so we
              can free list later */

   for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
               if (ifa->ifa_addr == NULL)
                   continue;

               family = ifa->ifa_addr->sa_family;

               /* Display interface name and family (including symbolic
                  form of the latter for the common families) */

               /* For an AF_INET* interface address, display the address */

         if ((family == AF_INET || family == AF_INET6) &&
              !strcmp( ifa->ifa_name, ifn))
         {
          s =  ((struct sockaddr_in *)ifa->ifa_netmask)->sin_addr.s_addr;
          break;
         }
    }
    return(s);
}


// ============================================================================
// Get default gateway and its metric for interface with given name
int getdefaultgateway(const char *ifn, int* metric = NULL)
{ // do not provide metric, if you don't need it
    long d, g;
    int v = 0;
    char buf[256];
    char iname[IFNAMSIZ];
    int line = 0;
    FILE * f;
    char * p;
    if( !ifn)
        return 0;
    // trying to obtain info from current routing table
    f = fopen("/proc/net/route", "r");
    if(!f)
        return 0;
    while(fgets(buf, sizeof(buf), f)) {
        if(line > 0) { // skip header
            p = buf;
            memset(iname,0,IFNAMSIZ);
            // get iface name
            while(*p && !isspace(*p)) {
                iname[p-buf] = *p;
                p++;
            }
            // check interface name
            if( strcmp( ifn, iname) != 0)
              continue;
            // skip spaces
            while(*p && isspace(*p))
                p++;
            // get destination and gateway
            if(sscanf(p, "%lx%lx", &d, &g)==2) {
                if(d == 0) { // default gw for our interface
                    // scan for metric
                    if( metric)
                      sscanf(p, "%lx%lx%d%d%d%d", &d,&d,&v,&v,&v,metric);
                    fclose(f);
                    return int(g);
                }
                else if (metric && d != 0) {
                    *metric = 0;
                }
            }
        }
        line++;
    }
    // default route not found !
    if(f)
        fclose(f);
    // Try to get metric (at least) from config
    if(metric) {
      if( !strcmp(ifn,LTE)) // fix for 3g iface
              sprintf(buf, "uci get network.wan.metric");
      else
              sprintf(buf, "uci get network.%s.metric", ifn);
      f = popen( buf, "r");
      if( f) {
              *metric = fgetc(f); // getting 1 char
              if( *metric < 0x30 || *metric > 0x39) // not a number
                *metric = 0;
              else
                *metric -= 0x30; // atoi
              pclose(f);
            }
    }
    return 0;
}


// ============================================================================
void readInterface(int fd, char butNum, int update = 1)
{
    struct in_addr ip_addr, mask_addr, gate_addr, doc_addr;
    switch(butNum)
    {
    case SATELLITE_BUT:
        ip_addr.s_addr = getIfaceAddr(SAT);

        if (strcmp(strWithoutButtons[SAT_IP_STR],
            ip_addr.s_addr ? inet_ntoa(ip_addr) : NOIP_STRING) != 0)
        {
            strncpy(strWithoutButtons[SAT_IP_STR],
                    ip_addr.s_addr ? inet_ntoa(ip_addr) : NOIP_STRING,
                    STRINGS_LINE);
            if( update)
              strings(fd, strWithoutButtons[SAT_IP_STR], SAT_IP_STR);
        }
        break;
    case ETHERNET_0_BUT:        
        ip_addr.s_addr = getIfaceAddr(ETH_0);
        mask_addr.s_addr = getIfaceMask(ETH_0);
        gate_addr.s_addr = getdefaultgateway(ETH_0);

        if (strcmp(strWithButtons[IP_ETH_0_BUT],
            ip_addr.s_addr ? inet_ntoa(ip_addr) : NOIP_STRING) != 0)
        {
            strncpy(strWithButtons[IP_ETH_0_BUT],
                    ip_addr.s_addr ? inet_ntoa(ip_addr) : NOIP_STRING,
                    STRINGS_LINE);
            if( update) strings(fd, strWithButtons[IP_ETH_0_BUT], IP_ETH_0_BUT);
        }

        if (strcmp(strWithButtons[MASK_ETH_0_BUT],
            mask_addr.s_addr ? inet_ntoa(mask_addr) : NOIP_STRING) != 0)
        {
            strncpy(strWithButtons[MASK_ETH_0_BUT],
                   mask_addr.s_addr ? inet_ntoa(mask_addr) : NOIP_STRING,
                   STRINGS_LINE);
            if( update) strings(fd, strWithButtons[MASK_ETH_0_BUT], MASK_ETH_0_BUT);
        }

        if (strcmp(strWithButtons[GATE_ETH_0_BUT],
            gate_addr.s_addr ? inet_ntoa(gate_addr) : NOIP_STRING) != 0)
        {
            strncpy(strWithButtons[GATE_ETH_0_BUT],
                    gate_addr.s_addr ? inet_ntoa(gate_addr) : NOIP_STRING,
                    STRINGS_LINE);
            if( update) strings(fd, strWithButtons[GATE_ETH_0_BUT], GATE_ETH_0_BUT);
        }
        break;
    case ETHERNET_1_BUT:
        ip_addr.s_addr = getIfaceAddr(ETH_1);
        mask_addr.s_addr = getIfaceMask(ETH_1);
        gate_addr.s_addr = getdefaultgateway(ETH_1);


        if (strcmp(strWithButtons[IP_ETH_1_BUT],
            ip_addr.s_addr ? inet_ntoa(ip_addr) : NOIP_STRING) != 0)
        {
            strncpy(strWithButtons[IP_ETH_1_BUT],
                    ip_addr.s_addr ? inet_ntoa(ip_addr) : NOIP_STRING,
                    STRINGS_LINE);
            if( update) strings(fd, strWithButtons[IP_ETH_1_BUT], IP_ETH_1_BUT);
        }

        if (strcmp(strWithButtons[MASK_ETH_1_BUT],
            mask_addr.s_addr ? inet_ntoa(mask_addr) : NOIP_STRING) != 0)
        {
            strncpy(strWithButtons[MASK_ETH_1_BUT],
                    mask_addr.s_addr ? inet_ntoa(mask_addr) : NOIP_STRING,
                    STRINGS_LINE);
            if( update) strings(fd, strWithButtons[MASK_ETH_1_BUT], MASK_ETH_1_BUT);
        }

        if (strcmp(strWithButtons[GATE_ETH_1_BUT],
            gate_addr.s_addr ? inet_ntoa(gate_addr) : NOIP_STRING) != 0)
        {
            strncpy(strWithButtons[GATE_ETH_1_BUT],
                    gate_addr.s_addr ? inet_ntoa(gate_addr) : NOIP_STRING,
                    STRINGS_LINE);
            if( update) strings(fd, strWithButtons[GATE_ETH_1_BUT], GATE_ETH_1_BUT);
        }
        break;
    case WIFI_BUT:
        ip_addr.s_addr = getIfaceAddr(WIFI);
        strncpy(strWithoutButtons[WIFI_IP_STR],
            ip_addr.s_addr ? inet_ntoa(ip_addr) : NOIP_STRING,
            STRINGS_LINE);
        break;
    case LTE_BUT:
        ip_addr.s_addr = getIfaceAddr(LTE);
        strncpy(strWithoutButtons[LTE_IP_STR],
            ip_addr.s_addr ? inet_ntoa(ip_addr) : NOIP_STRING,
            STRINGS_LINE);
        break;
    case CONTROL_BUT:
        ip_addr.s_addr = getIfaceAddr(LOC);
        mask_addr.s_addr = getIfaceMask(LOC);
        doc_addr.s_addr = getIfaceAddr(DOC);
        if (strcmp(strWithButtons[IP_LOCAL_BUT],
            ip_addr.s_addr ? inet_ntoa(ip_addr) : NOIP_STRING) != 0)
        {
          strncpy(strWithButtons[IP_LOCAL_BUT],
            ip_addr.s_addr ? inet_ntoa(ip_addr) : NOIP_STRING, STRINGS_LINE);
          
          if( update) strings(fd, strWithButtons[IP_LOCAL_BUT], IP_LOCAL_BUT);
        }
        
        if (strcmp(strWithButtons[MASK_LOCAL_BUT],
            mask_addr.s_addr ? inet_ntoa(mask_addr) : NOIP_STRING) != 0)
        {
          strncpy(strWithButtons[MASK_LOCAL_BUT],
            mask_addr.s_addr ? inet_ntoa(mask_addr) : NOIP_STRING, STRINGS_LINE);
          if( update) strings(fd, strWithButtons[MASK_LOCAL_BUT], MASK_LOCAL_BUT);
	    }
            
        if (strcmp(strWithButtons[IP_DOC_SERVER_BUT],
            doc_addr.s_addr ? inet_ntoa(doc_addr) : NOIP_STRING) != 0)
        {
          strncpy(strWithButtons[IP_DOC_SERVER_BUT],
            doc_addr.s_addr ? inet_ntoa(doc_addr) : NOIP_STRING, STRINGS_LINE);
          if( update) strings(fd, strWithButtons[IP_DOC_SERVER_BUT],
                              IP_DOC_SERVER_BUT);
		}
        break;
    }
}


// ============================================================================
void startUp(int fd)
{
    int priority[CH_NUM];
    memset( priority, 0 ,sizeof(priority));

    for(int i = 0; i < CH_NUM; ++i)
    {
        getdefaultgateway(CH_NAMES[i], &priority[i]);
        sprintf(strWithButtons[SAT_PRIORITY_BUT + i], "%d", priority[i]);
    }
    readInterface(fd, SATELLITE_BUT,0);
    readInterface(fd, ETHERNET_0_BUT,0);
    readInterface(fd, ETHERNET_1_BUT,0);
    readInterface(fd, WIFI_BUT,0);
    readInterface(fd, LTE_BUT,0);
    readInterface(fd, CONTROL_BUT,0);

    changedForm(fd, STATISTICS_BUT + 1);
}


// ============================================================================
int main(int argc, char *argv[])
{
    int argument = 0;
    int fd;
    //memset(deviceName, 0, sizeof(deviceName));
    sprintf(deviceName,"%s",DEVICE_NAME);
    memset(strWithButtons, 0, sizeof(strWithButtons));
    memset(strWithoutButtons, 0, sizeof(strWithoutButtons));
    struct termios oldtio;

    // ------------------------------------------------------------------------
    getVersion();
    // ------------------------------------------------------------------------
    while ((argument = getopt(argc, argv, "vn:")) != -1)
    {
        switch(argument)
        {
        case 'v':
            outputEnable = 1;
            break;
        case 'n':
            strncpy(deviceName,optarg, sizeof(deviceName));
            //sscanf(deviceName, "%s", optarg);
            break;
        }
    }
    // ------------------------------------------------------------------------
	openlog( argv[0], 0,  LOG_USER); // prepare to log
    fd = open(deviceName, O_RDWR | O_NOCTTY | O_NONBLOCK);

    if (fd < 0)
    {
        Printf(">> Error to connect device!\n\n");
        syslog( LOG_USER|LOG_INFO, "Error to connect device.");
        exit(1);
    }

        Printf(">> Device connected!\n\n");
        syslog( LOG_USER|LOG_INFO, "Device connected.");
        struct termios newtio;
        struct sigaction saio;               //definition of signal action

        // --------------------------------------------------------------------
        //install the serial handler before making the device asynchronous
        saio.sa_handler = signal_handler_IO;
        sigemptyset(&saio.sa_mask);   //saio.sa_mask = 0;
        saio.sa_flags = 0;
        saio.sa_restorer = NULL;
        sigaction(SIGIO,&saio,NULL);
        
        //install the child handler
        saio.sa_handler = signal_handler_CHLD;
        sigemptyset(&saio.sa_mask);   //saio.sa_mask = 0;
        saio.sa_flags = 0;
        saio.sa_restorer = NULL;
        sigaction(SIGCHLD,&saio,NULL);

        //install the FINAL handler before making the device asynchronous
        saio.sa_handler = signal_handler_TERM;
        sigemptyset(&saio.sa_mask);   //saio.sa_mask = 0;
        saio.sa_flags = 0;
        saio.sa_restorer = NULL;
        sigaction(SIGTERM,&saio,NULL);

        saio.sa_handler = signal_handler_TERM;
        sigemptyset(&saio.sa_mask);   //saio.sa_mask = 0;
        saio.sa_flags = 0;
        saio.sa_restorer = NULL;
        sigaction(SIGINT, &saio, NULL);

        // allow the process to receive SIGIO
        fcntl(fd, F_SETOWN, getpid());
        // Make the file descriptor asynchronous (the manual page says only
        // O_APPEND and O_NONBLOCK, will work with F_SETFL...)
        // fcntl(fd, F_SETFL, FASYNC); Roman to enable select

        tcgetattr(fd, &oldtio); // save current port SETTINGS
        // cfmakeraw(&newtio);
        // set new port SETTINGS for raw input processing
        bzero(&newtio, sizeof(newtio));
        newtio.c_cflag = BAUD | DATABITS | STOPBITS | PARITYON |
                         PARITY | CLOCAL | CREAD;
        newtio.c_iflag = IGNPAR;
        newtio.c_oflag = 0;
        newtio.c_lflag = 0;       //ICANON;
        newtio.c_cc[VMIN]=1;
        newtio.c_cc[VTIME]=0;
        // cfmakeraw(&newtio);
        tcflush(fd, TCIOFLUSH);
        tcsetattr(fd, TCSANOW, &newtio);

        // --------------------------------------------------------------------
        startUp(fd);

        // loop while waiting for input.
        // normally we would do something useful here
        while (STOP == false)
        {
            fd_set fds;
            int sel = 0;
            struct timeval tv;
            // Calling select to check input available
            FD_ZERO( &fds);
            FD_SET(fd, &fds);
            tv.tv_sec = atoi(updatePeriod);
            tv.tv_usec = 0;
            sel = select(fd + 1, &fds, NULL, NULL, &tv);

            // Retrievieng select return value
            if(sel < 0) { // signal comt
                if (wait_flag) {
					int status;
                    wait( &status);
                    wait_flag = false;
				}
                continue;
			}

            // ----------------------------------------------------------------
            // Обновление раз в 3 секунды
            if (sel == 0 && currForm > 0 && !isFormChanging)
            { // постоянно рисуем графики
                setScope(fd, 0, getCPU());
                setScope(fd, 1, getMemory());

                readInterface(fd, currForm-1);
                formReloading(fd, currForm);
                continue;
            }
            // ----------------------------------------------------------------

            if(!FD_ISSET(fd,&fds))
                continue;

            // Тут-то все и начинается...            
            readFromDevice(fd);
        }
    // Применим все сделанные сетевые настройки
    system("uci commit network");

	close(fd);
    closelog();
    return 0;
}
